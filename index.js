
const Multer=require('multer');
// require ================================================================

var         express = require("express"),                   // web development framework
            mustacheExpress = require('mustache-express');  // Logic-less {{mustache}} templates

 

// express ===============================================================

var app = express();

// configure =============================================================
app.engine('html', mustacheExpress());          // register file extension mustachae

app.set('view engine', 'html');                 // register file extension for partials
app.set('views', __dirname + '/html');

app.use(express.static(__dirname + '/public')); // set static folder

var storage = Multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public");
  },
  filename: function (req, file, cb) {
    var videoPath = file.fieldname + "-" + Date.now()+"-"+file.originalname;
    req.videoPath = "uploads/tmp/video/" + videoPath;
    cb(null, videoPath);
  },
});
const multer = Multer({
  storage: storage,
  limits: {
    fileSize: 50000 * 1024 * 1024, // no larger than 5mb, you can change as needed.
  },
});

// routes =================================================================
app.get('/', function(req, res) {
    res.render('master', {
          
        });
    });
app.post("/upload/image",multer.single('image'),(req,response)=>{
  console.log(req.file)
  let payload={
    image:req.file.filename,
    firstname:req.body.firstname,
    age:req.body.age,
  }
   response.send(payload)
})


app.listen(process.env.PORT || 3000);

 

 